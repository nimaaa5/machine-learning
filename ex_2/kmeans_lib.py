from sklearn.cluster import KMeans
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def load_data_set(file_path):
    df = pd.read_csv(file_path, header=None)
    x1 = df.iloc[:, 0].values
    x2 = df.iloc[:, -1].values
    return x1, x2


if __name__ == '__main__':
    x1, x2 = load_data_set('data/dataset_kmeans_5.txt')
    X = np.array(list(zip(x1, x2)))
    k = 7
    kmeans = KMeans(n_clusters=k, random_state=1).fit(X)
    colors = ['r', 'g', 'b', 'y', 'c', 'm', 'pink', 'blue']
    fig, ax = plt.subplots()
    for i in range(k):
        points = np.array([X[j] for j in range(len(X)) if kmeans.labels_[j] == i])
        ax.scatter(points[:, 0], points[:, 1], s=100, c=colors[i])
    ax.scatter(kmeans.cluster_centers_[:, 0], kmeans.cluster_centers_[:, 1], marker='*', s=100, c='#050505')
    plt.show()
