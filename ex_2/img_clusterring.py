#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 12 09:21:46 2019

@author: nima
"""
from PIL import Image
import numpy as np
from sklearn.cluster import KMeans

img = Image.open('data/ShahBaz-Bird.jpg')
pixels = np.array(img)
l = []
for i in range(444):
    for j in range(709):
        l.append(pixels[i][j])

model = KMeans(n_clusters=15)
model.fit(l)

centroid = np.round(model.cluster_centers_[:, :])

for i in range(444):
    for j in range(709):
        pixels[i][j] = centroid[model.labels_[i * 709 + j], :]

img = Image.fromarray(pixels)
img.show()
