from sklearn.metrics import confusion_matrix
from sklearn.model_selection import cross_val_score, cross_val_predict
from sklearn.neighbors import KNeighborsClassifier
from sklearn import preprocessing
import collections
import math
import numpy as np
import pandas as pd
from enum import Enum
import time

""" متد نوشته شده برای محاسبه ی الگوریتم knn که سه متغیر وروی خواسته شده در صورت سوال در آن آمده است."""


def start_knn(k_fold, dataset_address, distance_metrics):
    dataset = pd.read_csv(dataset_address,
                          sep="\t",
                          header=None)
    x = dataset.values  # returns a numpy array

    min_max_scaler = preprocessing.MinMaxScaler()
    x_scaled = min_max_scaler.fit_transform(x)
    dataset = pd.DataFrame(x_scaled)
    print(dataset)
    knn = Knn()
    conf_matrix = np.zeros((3, 3))
    for i in range(0, k_fold):
        predictions = []
        training, validation = k_fold_cross_validation(dataset, k_fold, i)
        x_train = training.iloc[:, 0:7].values
        y_train = training.iloc[:, 7].values
        x_test = validation.iloc[:, 0:7].values
        y_test = validation.iloc[:, 7].values
        knn.calculate_knn(x_train, x_test, y_train, predictions, 5, distance_metrics)
        get_confusion_matrix(y_test, predictions, conf_matrix)
    print(conf_matrix)
    print(get_accuracy(conf_matrix) * 100)


""" محاسبه ی ماتریس پریشانی در این متد آمده است ."""


def get_confusion_matrix(y_test, predictions, matrix):
    for i in range(0, len(y_test)):
        actual_index = y_test[i]
        pred_index = predictions[i]
        if actual_index == 0:
            actual_index = 1
        elif actual_index == .5:
            actual_index = 2
        elif actual_index == 1:
            actual_index = 3
        if pred_index == 0:
            pred_index = 1
        elif pred_index == .5:
            pred_index = 2
        elif pred_index == 1:
            pred_index = 3
        matrix[actual_index - 1][pred_index - 1] += 1


""" محاسبه ی دقت الگوریتم در این متد آمده است ."""


def get_accuracy(matrix):
    total = 0
    diameter = 0
    for i in range(len(matrix)):
        for j in range(len(matrix)):
            total += matrix[i][j]
            if i == j:
                diameter += matrix[i][j]
    return diameter / total


def k_fold_cross_validation(dataset, k_fold, i):
    # dataset = dataset.sample(frac=1).reset_index(drop=True)
    # print(dataset)
    split = math.floor(dataset.shape[0] / k_fold)
    training = dataset.drop(dataset.index[i * split:i * split + split])
    validation = dataset[i * split:i * split + split]
    return training, validation


""" یک کلاس اینام برای انتخاب معیار فاصله"""


class DistanceMetrics(Enum):
    EUCLIDEAN = 1
    MANHATTAN = 2
    COSINE_SIMILARITY = 3


""" کلاس knn که برای محاسبه و پیش بینی نوشته شده است"""


class Knn:

    def calculate_knn(self, x_train, x_test, y_train, predictions, k, distance_metrics):
        if k > len(x_train):
            raise ValueError

        for i in range(len(x_test)):
            predictions.append(self.predict(x_train, x_test[i, :], y_train, k, distance_metrics))

    def predict(self, x_train, x_test, y_train, k, distance_metrics):
        distances = []
        targets = []

        if distance_metrics == DistanceMetrics.EUCLIDEAN:
            for i in range(len(x_train)):
                distance = self.get_euclidean_distance(x_train, x_test, i)
                distances.append([distance, i])
        elif distance_metrics == DistanceMetrics.MANHATTAN:
            for i in range(len(x_train)):
                distance = self.get_manhattan_distance(x_train, x_test, i)
                distances.append([distance, i])
        elif distance_metrics == DistanceMetrics.COSINE_SIMILARITY:
            for i in range(len(x_train)):
                distance = self.get_cos_distance(x_train, x_test, i)
                distances.append([distance, i])

        distances = sorted(distances)

        for i in range(k):
            index = distances[i][1]
            targets.append(y_train[index])
        return collections.Counter(targets).most_common(1)[0][0]

    @staticmethod
    def get_euclidean_distance(x_train, x_test, i):
        return np.sqrt(np.sum(np.square(x_test - x_train[i, :])))

    @staticmethod
    def get_manhattan_distance(x_train, x_test, i):
        return np.sum(np.abs(x_test - x_train[i, :]))

    @staticmethod
    def get_cos_distance(x_train, x_test, i):
        return (np.dot(x_test, x_train[i, :])) / (np.sqrt(np.sum(np.square(x_test))) * np.sqrt(
            np.sum(np.square(x_train[i, :]))))
        # return 1 - spatial.distance.cosine(x_test, x_train[i, :])


""" این متد برای استفاده از لایبرری scikit_learn  برای محاسبه ی الگوریتم KNN 
 اسفتفاده می کند که در صورت سوال خواسته شده بود"""


def start_knn_library(dataset_address):
    dataset = pd.read_csv(dataset_address,
                          sep="\t",
                          header=None)
    X_train = dataset.iloc[:, 0:7].values
    y_train = dataset.iloc[:, 7].values
    knn = KNeighborsClassifier(n_neighbors=5, metric='euclidean', p=2)
    y_pred = cross_val_predict(knn, X_train, y_train, cv=10)
    conf_mat = confusion_matrix(y_train, y_pred)
    print(conf_mat)
    scores = cross_val_score(knn, X_train, y_train, cv=10, scoring='accuracy')
    print(scores.mean() * 100)


if __name__ == '__main__':
    address = "/media/nima/1EFEA14EFEA11ED11/MS_Computer_Science/Machine Learning/HW2/dataset.txt"
    """در متد موارد خواسته شده در صورت سوال وارد می شود
     پارامتر اول مقدار k-fold
     پارامتر دوم آدرس دیتاست
     پارامتر سوم که enum است برای مشخص کردن معیار فاصله"""

    k_f = 10
    start_time = time.time()
    start_knn(k_f, address, DistanceMetrics.EUCLIDEAN)
    print("--- %s seconds ---" % (time.time() - start_time))
    # start_knn_library(address)
