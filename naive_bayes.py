import pandas as pd
import numpy as np
import math
from sklearn.model_selection import train_test_split
from collections import Counter


class NaiveBayes:

    def __init__(self, X_tr, y_tr):
        self.X_train = X_tr
        self.y_train = y_tr
        self.labels = list(Counter(y_train).keys())

    def predict(self, X_tst):
        predicted_values = []
        for i in range(0, len(X_tst)):
            probabilities = self.calculate_probability_y_x(X_tst[i, :])
            predicted_values.append(self.get_arg_max(probabilities))
        return predicted_values

    def calculate_probability_y_x(self, row):

        probs = {}
        for label in self.labels:
            p_y = self.calculate_probability_y(Counter(y_train).get(label))
            p_x_y = np.zeros(len(row))
            j = 0
            for feature in row:
                for i in range(0, len(X_train)):
                    if feature == X_train[i][j]:
                        if y_train[i] == label:
                            p_x_y[j] += 1
                j += 1
            if self.has_zero(p_x_y):
                self.smooth(p_x_y)
            p_x_y /= Counter(y_train).get(label)
            self.calculate_log(p_x_y)
            probs[label] = self.calculate_numerator(p_y, p_x_y)
        return probs

    def calculate_probability_y(self, number_of_label):
        return math.log(number_of_label / len(self.y_train))

    @staticmethod
    def calculate_numerator(p_y, p_x_y):
        sum = 0
        for i in range(0, len(p_x_y)):
            sum += p_x_y[i]
        return sum + p_y

    @staticmethod
    def has_zero(p_x_y):
        for i in range(0, len(p_x_y)):
            if p_x_y[i] == 0:
                return True
        return False

    @staticmethod
    def smooth(p_x_y):
        for i in range(0, len(p_x_y)):
            p_x_y[i] += 1

    @staticmethod
    def calculate_log(p_x_y):
        for i in range(0, len(p_x_y)):
            p_x_y[i] = math.log(p_x_y[i])
        return p_x_y

    def get_arg_max(self, probabilities):
        maximum = -100
        class_name = ''
        for i in range(0, len(probabilities)):
            if maximum < probabilities.get(self.labels[i]):
                maximum = probabilities.get(self.labels[i])
                class_name = self.labels[i]
        return class_name


# این کلاس به منظور ارزیابی دقت دسته بند مورد استفاده قرار می گیرد.
class Evaluation:

    def __init__(self, y_tst, y_prd):
        self.y_test = y_tst
        self.y_pred = y_prd
        self.labels = Counter(list(Counter(y_train).keys()))
        self.label_dict = self.get_label_dict()
        self.number_of_labels = len(self.labels)
        self.confusion_matrix = np.zeros((self.number_of_labels, self.number_of_labels), dtype=int)
        print(self.label_dict)

    # در کانستراکتور این کلاس تعداد تکرار ، ضریب یادگیری و
    # لیستی از هزینه ها و هایپوتسیس ها در نظر گرفته شده است
    def get_label_dict(self):
        d = {}
        i = 0
        for label in self.labels:
            d[label] = i
            i += 1
        return d

    def get_confusion_matrix(self):
        for i in range(0, len(self.y_test)):
            self.confusion_matrix[self.label_dict[self.y_test[i]]][self.label_dict[self.y_pred[i]]] += 1
        return self.confusion_matrix

    def get_sensitivity(self, label):
        index = self.label_dict[label]
        _sum = 0
        for i in range(0, len(self.confusion_matrix[index])):
            _sum += self.confusion_matrix[index][i]
        return self.confusion_matrix[index][index] / _sum

    def get_specificity(self, label):
        index = self.label_dict[label]
        TN = 0
        _sum = 0
        for i in range(0, len(self.confusion_matrix[index])):
            for j in range(0, len(self.confusion_matrix[index])):
                if i != index and j != index:
                    TN += self.confusion_matrix[i][j]

        for i in range(0, len(self.confusion_matrix[index])):
            if i != index:
                _sum += self.confusion_matrix[i][index]
        return TN / (TN + _sum)

    def get_FP(self, label):
        index = self.label_dict[label]
        FP = 0
        for i in range(0, len(self.confusion_matrix[index])):
            if index != i:
                FP += self.confusion_matrix[i][index]
        return FP

    def get_FN(self, label):
        index = self.label_dict[label]
        FN = 0
        for i in range(0, len(self.confusion_matrix[index])):
            if index != i:
                FN += self.confusion_matrix[index][i]
        return FN


def load_file(file):
    data_frame = pd.read_csv(file, sep=',', header=None)
    print(len(data_frame))
    X = data_frame.iloc[:, 0:6].values
    y = data_frame.iloc[:, -1].values
    return X, y


def split_dataset(X, y, test_ratio):
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_ratio, random_state=0)
    return X_train, X_test, y_train, y_test


if __name__ == '__main__':
    file_path = 'car.data'
    test_size = 0.3
    X, y = load_file(file_path)
    print(y)
    X_train, X_test, y_train, y_test = split_dataset(X, y, test_size)
    y_pred = NaiveBayes(X_train, y_train).predict(X_test)
    eval = Evaluation(y_test, y_pred)
    print(eval.get_confusion_matrix())
    print('Sensitivity : ' + str(eval.get_sensitivity('good')))
    print('Specificity : ' + str(eval.get_specificity('good')))
    print('FP : ' + str(eval.get_FP('good')))
    print('FN : ' + str(eval.get_FN('good')))
