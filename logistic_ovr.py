#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 23 08:35:20 2019

@author: nima
"""

import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
import operator
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix

#لود کردن دیتاست
def load_train_test_set():
    train_data = np.load('train-ubyte.npz')
    test_data = np.load('test-ubyte.npz')

    x_train, y_train = train_data['a'], train_data['b']
    x_test, y_test = test_data['a'], test_data['b']
    return x_train, y_train, x_test, y_test


# تبدیل پیکسل های عکس که دو بعدی هستند به‌ آرایه ی یک بعدی برای اینکه الگوریتم کتابخانه بتواند مدل مورد نظر را بسازد
def convert_tensor_to_matrix(tensor, scaler):
    x = []
    for row in tensor:
        x.append(row.ravel())
    return x

#ساخت ۱۰ لیست از باینری که هر بار یک کلاس را کلاس هدف(۱) و بقیه کلاس ها را کلاس غیر هدف(۰) در نظر می گیریم
def create_one_vs_rest(y_train):
    y_arrays = np.zeros((10, len(y_train)), dtype=int)
    for i in range(0, 10):
        for j in range(0, len(y_train)):

            if y_train[j] == i:
                y_arrays[i][j] = 1
            else:
                y_arrays[i][j] = 0
    return y_arrays

#ساختن ۱۰ مدل و فیت کردن الگوریتم
def create_models(y_arr):
    models = []
    for y_row in y_arr:
        model = LogisticRegression(solver='lbfgs')
        models.append(model.fit(x_train, y_row))
    return models

#بدست آوردن احتمالا مختلف برای هر داده ی تست برای هر ۱۰ مدل
def create_probability_list(models):
    arr = []
    for model in models:
        arr.append(model.predict_proba(x_test))
    return arr

# محاسبه ی مقدار پیش بینی شده پس از اینکه مقدار احتمال ماکزیمم هر کلاس بدست آمد
def predict(x_test, prob_list):
    max_list=[]
    y_pred = []
    for i in range(0, len(x_test)):
        for j in range(0, len(prob_list)):
            max_list.append(prob_list[j][i][1])
        index, value = max(enumerate(max_list), key=operator.itemgetter(1))
        y_pred.append(index)      
        max_list.clear()
    return y_pred

if __name__ == '__main__':
    x_train, y_train, x_test, y_test = load_train_test_set()
    scaler = StandardScaler()
    x_train = convert_tensor_to_matrix(x_train, scaler)
    x_test = convert_tensor_to_matrix(x_test, scaler)

    y_train_list = create_one_vs_rest(y_train)
    
    models = create_models(y_train_list)

    i=0
    sc=[]
    for model in models:
        sc.append(model.score(x_train, y_train_list[i]))
        i += 1
    
    training_score = sum(sc)/len(sc)
    
    prob_list = create_probability_list(models)

    y_pred = predict(x_test, prob_list)
    print(confusion_matrix(y_test, y_pred))
    print(accuracy_score(y_test, y_pred))

    import matplotlib.pyplot as plt
    x_train, y_train, x_test, y_test = load_train_test_set()
    
    plt.figure(figsize=(20,20))
    for  i in range(0,100):
        plt.subplot(5, 12, i + 1).axis('off')
        plt.imshow(x_test[i])
        plt.title('pred ' +str(y_pred[i]) + '/org ' + str(y_test[i]))
    