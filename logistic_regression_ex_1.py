import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
from collections import Counter
from sklearn.metrics import accuracy_score


# این کلاس به منظور ارزیابی دقت دسته بند مورد استفاده قرار می گیرد.
class Evaluation:

    def __init__(self, y_tst, y_prd):
        self.y_test = y_tst
        self.y_pred = y_prd
        self.labels = Counter(list(Counter(y_train).keys()))
        self.label_dict = self.get_label_dict()
        self.number_of_labels = len(self.labels)
        self.confusion_matrix = np.zeros((self.number_of_labels, self.number_of_labels), dtype=int)
        print(self.label_dict)

    # برچسب های متغیر وابسته به عدد تبدیل می شوند و در یک دیتا دیکشنری قرار می گیرند تا محاسبه ی ماتریس کانفیوژن آسان شود.
    def get_label_dict(self):
        d = {}
        i = 0
        for label in self.labels:
            d[label] = i
            i += 1
        return d

    def get_confusion_matrix(self):
        for i in range(0, len(self.y_test)):
            self.confusion_matrix[self.label_dict[self.y_test[i]]][self.label_dict[self.y_pred[i]]] += 1
        return self.confusion_matrix

    def get_sensitivity(self, label):
        index = self.label_dict[label]
        _sum = 0
        for i in range(0, len(self.confusion_matrix[index])):
            _sum += self.confusion_matrix[index][i]
        return self.confusion_matrix[index][index] / _sum

    def get_specificity(self, label):
        index = self.label_dict[label]
        TN = 0
        _sum = 0
        for i in range(0, len(self.confusion_matrix[index])):
            for j in range(0, len(self.confusion_matrix[index])):
                if i != index and j != index:
                    TN += self.confusion_matrix[i][j]

        for i in range(0, len(self.confusion_matrix[index])):
            if i != index:
                _sum += self.confusion_matrix[i][index]
        return TN / (TN + _sum)

    def get_FP(self, label):
        index = self.label_dict[label]
        FP = 0
        for i in range(0, len(self.confusion_matrix[index])):
            if index != i:
                FP += self.confusion_matrix[i][index]
        return FP

    def get_FN(self, label):
        index = self.label_dict[label]
        FN = 0
        for i in range(0, len(self.confusion_matrix[index])):
            if index != i:
                FN += self.confusion_matrix[index][i]
        return FN


class LogisticRegression:

    # در کانستراکتور این کلاس تعداد تکرار ، ضریب یادگیری و
    # لیستی از هزینه ها و هایپوتسیس ها در نظر گرفته شده است
    def __init__(self, lr=1, num_iter=1000, fit_intercept=False, _lambda=0.3):
        self.lr = lr
        self.num_iter = num_iter
        self.fit_intercept = fit_intercept
        self.costs = []
        self.hh = []
        self._lambda = _lambda

    # یک ستون یک به ماتریس فیچر ها کانکت می کند.
    @staticmethod
    def __add_intercept(X):
        intercept = np.ones((X.shape[0], 1))
        return np.concatenate((intercept, X), axis=1)

    # محاسبه ی تابع سیگموید
    @staticmethod
    def sigmoid(z):
        return 1 / (1 + np.exp(-z))

    # محاسبه ی تابع هزینه
    @staticmethod
    def cost(h, y):
        return -y * np.log(h) - (1 - y) * np.log(1 - h)

    # ساخت مدل با ماتریس فیچر ها و متغیر وابسته
    def fit(self, X, y):
        if self.fit_intercept:
            X = self.__add_intercept(X)

        self.theta = np.ones(len(X[0]))
        for i in range(self.num_iter):
            z = np.dot(X, self.theta)
            h = self.sigmoid(z)
            gradient = np.dot(X.T, (h - y)) / y.size
            self.theta -= (self.lr * gradient + self._lambda * sum(self.theta))

            z = np.dot(X, self.theta)
            h = self.sigmoid(z)
            print(f'cost: {self.cost(h, y)} \t')
            self.costs.append(self.cost(h, y))
            self.hh.append(h)
        #     در این قسمت نمودار تابع هزینه پلات می شود
        plt.plot(self.hh, self.costs)
        plt.xlabel('h(x)')
        plt.ylabel('cost')
        plt.show()

    def predict_prob(self, X):
        if self.fit_intercept:
            X = self.__add_intercept(X)
        return self.sigmoid(np.dot(X, self.theta))

    # در این تابع با دادن ترشهولد و ماتریس ویژگی های تست مقدار از روی مدل بدست آمده پیش بینی می شود
    def predict(self, X, threshold):
        return self.predict_prob(X) >= threshold


def load_file(file):
    return pd.read_csv(file, sep=',', header=None)


def separate_x_y(df):
    X = df.iloc[:, 1: 11].values
    y = df.iloc[:, -1].values
    return X, y


if __name__ == '__main__':
    file_path = 'breast-cancer-wisconsin.data'
    data_frame = load_file(file_path)
    for i in range(0, len(data_frame)):
        if data_frame.iloc[i, 10] == 2:
            data_frame.iloc[i, 10] = 0
        else:
            data_frame.iloc[i, 10] = 1
    data_frame = data_frame.drop(data_frame.columns[6], axis=1)
    print(data_frame)
    train, validate, test = np.split(data_frame.sample(frac=1),
                                     [int(.6 * len(data_frame)), int(.8 * len(data_frame))])
    X_train, y_train = separate_x_y(train)
    sc = StandardScaler()
    X_train = sc.fit_transform(X_train)
    X_test, y_test = separate_x_y(test)
    X_test = sc.fit_transform(X_test)

    model = LogisticRegression()
    model.fit(X_train, y_train)
    pred = model.predict(X_test, .5)
    pred1 = []
    print(pred)
    for i in range(0, len(pred)):
        if pred[i]:
            pred1.append(1)
        else:
            pred1.append(0)

    print(accuracy_score(y_test, pred))
    eval = Evaluation(y_test, pred1)
    print(eval.get_confusion_matrix())
    print('Sensitivity : ' + str(eval.get_sensitivity(1)))
    print('Specificity : ' + str(eval.get_specificity(1)))
    print('FP : ' + str(eval.get_FP(1)))
    print('FN : ' + str(eval.get_FN(1)))
