import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from copy import deepcopy
from sklearn.metrics import davies_bouldin_score


class KMeans:

    def __init__(self, k, x1, x2):
        self.k = k
        self.x1 = x1
        self.x2 = x2
        self.X = np.array(list(zip(x1, x2)))
        self.centroid_x = np.random.randint(np.min(x1), np.max(x1), size=k)
        self.centroid_y = np.random.randint(np.min(x2), np.max(x2), size=k)
        self.centroid = np.array(list(zip(self.centroid_x, self.centroid_y)), dtype=np.float32)
        self.sse = []
        self.davies_bouldin = []

    def calculate_clusters(self):
        old_centroid = np.zeros(self.centroid.shape)
        clusters = np.zeros(len(self.X))

        is_distance_zero = self.euclidean_distance(self.centroid, old_centroid, None) == 0
        c = 0
        while not is_distance_zero:
            distances = 0
            for i in range(0, len(self.X)):
                distances = self.euclidean_distance(self.X[i], self.centroid)
                cluster = np.argmin(distances)
                clusters[i] = cluster
            self.sse.append(np.sum(np.power(distances, 2)))

            old_centroid = deepcopy(self.centroid)

            for i in range(0, self.k):
                points = [self.X[j] for j in range(len(self.X)) if clusters[j] == i]
                self.centroid[i] = np.mean(points, axis=0)
            is_distance_zero = self.euclidean_distance(self.centroid, old_centroid, None) == 0
            print(str(c) + "is working...")
            self.davies_bouldin.append(davies_bouldin_score(self.X, clusters))
            c += 1
            self.plot(clusters)

    @staticmethod
    def euclidean_distance(x, y, ax=1):
        return np.linalg.norm(x - y, axis=ax)

    def plot_diagram(self):
        plt.scatter(self.x1, self.x2, c='#050505', s=50)
        plt.scatter(self.centroid_x, self.centroid_y, marker='*', s=200, c='white')
        plt.show()

    def plot_sse_diagram(self):
        iter = [[i] for i in range(0, len(self.sse))]
        plt.plot(iter, self.sse, label='SSE/Iteration')
        plt.xlabel('Iterations')
        plt.ylabel('SSE')
        plt.show()

    def plot(self, clusters):
        colors = ['g', 'r', 'b', 'y', 'c', 'm']
        fig, ax = plt.subplots()
        for i in range(self.k):
            points = np.array([self.X[j] for j in range(len(self.X)) if clusters[j] == i])
            ax.scatter(points[:, 0], points[:, 1], s=100, c=colors[i])
        ax.scatter(self.centroid[:, 0], self.centroid[:, 1], marker='*', s=200, c='white')
        fig.show()

    def plot_davies_bouldin(self):
        iter = [[i] for i in range(0, len(self.davies_bouldin))]
        plt.plot(iter, self.davies_bouldin, label='Davies_Bouldin_Index/Iteration')
        plt.xlabel('Iterations')
        plt.ylabel('Davies_Bouldin_Index')
        plt.show()


def load_data_set(file_path):
    df = pd.read_csv(file_path, header=None)
    x1 = df.iloc[:, 0].values
    x2 = df.iloc[:, -1].values
    return x1, x2


if __name__ == '__main__':
    x1, x2 = load_data_set('data/dataset_kmeans_4.txt')
    kmeans = KMeans(2, x1, x2)
    kmeans.plot_diagram()
    kmeans.calculate_clusters()
    kmeans.plot_sse_diagram()
    kmeans.plot_davies_bouldin()
