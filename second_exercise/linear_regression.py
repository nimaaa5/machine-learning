
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb 23 08:06:07 2019

@author: nima
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from numpy.linalg import inv
from sklearn import preprocessing



# =============================================================================
# def hypothesis(theta, X, deg, x_array):
#     h = np.ones((X.shape[0], 1))
#     theta = theta.reshape(1, deg + 1)
#     for i in range(0, X.shape[0]):
#         x_array = np.ones(deg + 1)
#         for j in range(0, deg + 1):
#             x_array[j] = pow(X[i], j)
#         x_array = x_array.reshape(deg + 1, 1)
#         h[i] = float(np.matmul(theta, x_array))
#     h = h.reshape(X.shape[0])
#     return h
# =============================================================================


def hypothesis(theta, X, deg):
      h = np.ones((X.shape[0], 1))
      theta = theta.reshape(1, (deg * len(X[0]))+ 1)
      for i in range(0, X.shape[0]):
          x_array = np.ones(deg * len(X[0])+ 1)
          for j in range(0, (deg * len(X[0]))):
                  x_array[j+1] = pow(X[i][j%len(X[0])], (j/len(X[0]))+1) 
                  print(x_array)
          x_array = x_array.reshape((deg * len(X[0]))+ 1, 1)
          print(x_array)
          h[i] = float(np.matmul(theta, x_array))
      h = h.reshape(X.shape[0])
      return h


def get_theta(X, deg, _lambda):
    x_array = np.ones((X.shape[0], (deg * len(X[0]))+ 1))
    for k in range(0, deg):
        for j in range(0, len(X[0])):
            for i in range(0, X.shape[0]):
                x_array[i][(k)*len(X[0]) +j] = pow(X[i][j],k+1) 
    scale_feature(x_array)
        
    theta = np.matmul(inv(np.matmul(x_array.transpose(), x_array)),
                       np.matmul(x_array.transpose(), y_train))
    return theta

def get_lambda(_lambda, deg):
    ar = np.zeros((deg+1, deg+1))
    for i in range(1, deg+1):
        for j in range(1, deg+1):
            if i==j:
                ar[i][j] = 1
    ar = np.dot(_lambda, ar)
    return ar

def calculate_mse(training_predictions, y_train):
    se = 0 
    for i in range(0,y_train.shape[0]):
        se = se + pow((training_predictions[i] - y_train[i]), 2)
    return se/y_train.shape[0]

def get_correlation(data):
    corr = data.corr()
    fig = plt.figure()
    ax = fig.add_subplot(111)
    cax = ax.matshow(corr,cmap='coolwarm', vmin=0, vmax=1)
    fig.colorbar(cax)
    ticks = np.arange(0,len(data.columns),1)
    ax.set_xticks(ticks)
    plt.xticks(rotation=90)
    ax.set_yticks(ticks)
    ax.set_xticklabels(data.columns)
    ax.set_yticklabels(data.columns)
    plt.show()

def remove_features(dataset):
    dataset = dataset.drop(['date', 'id'], axis=1)
    dataset['sqft'] = dataset['sqft_living']+dataset['sqft_lot']+dataset['sqft_above']+dataset['sqft_basement']
    dataset = dataset.drop(['sqft_living','sqft_lot','sqft_above','sqft_basement'], axis=1)
    dataset = dataset.drop(['zipcode', 'sqft_living15', 'sqft_lot15'], axis=1)
    dataset = dataset.drop(['condition', 'long'], axis=1)
    dataset = dataset.drop(['yr_built','yr_renovated'],axis=1)
    return dataset

def scale_feature(dataset):
    scaler = preprocessing.StandardScaler()
    scaled_df = scaler.fit_transform(dataset)
    scaled_df = pd.DataFrame(scaled_df, columns=dataset.columns)
    return scaled_df

dataset = pd.read_csv('data2_house_data.csv')
dataset = remove_features(dataset)
#dataset = scale_feature(dataset)
X_train = dataset.iloc[:, 1:len(dataset.columns)].values
y_train = dataset.iloc[:, 0].values

theta_3 = get_theta(X_train, 3, 0)

x = np.ones(25)
training_predictions_3 = hypothesis(theta_3, X_train, 3)

# =============================================================================
# x_array_5, theta_5 = get_theta(X_train, 5,0)
# x_array_7, theta_7 = get_theta(X_train, 7,0)
# 
# training_predictions_3 = hypothesis(theta_3, X_train, 3, x_array_3)
# training_predictions_5 = hypothesis(theta_5, X_train, 5, x_array_5)
# training_predictions_7 = hypothesis(theta_7, X_train, 7, x_array_7)
# scatter = plt.scatter(X_train, y_train, label="training data", color='orange')
# plt.plot(X_train, training_predictions_3,
#                            label="polynomial (degree 3) regression", color='red')
# plt.plot(X_train, training_predictions_5,
#                            label="polynomial (degree 5) regression", color='green')
# plt.plot(X_train, training_predictions_7,
#                            label="polynomial (degree 7) regression", color='blue')
# 
# =============================================================================
# =============================================================================
# print(calculate_mse(training_predictions_7, y_train))
# plt.legend()
# plt.xlabel('X Signal')
# plt.ylabel('Y Signal')
# =============================================================================















