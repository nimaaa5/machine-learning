import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def calculate_h(theta, degree, X):
    h = np.ones((len(X), 1))
    theta = theta.reshape(1, degree + 1)
    for i in range(0, len(X)):
        X_temp = np.ones(degree + 1)
        for j in range(0, degree + 1):
            X_temp[j] = pow(X[i], j)
        X_temp = X_temp.reshape(degree + 1, 1)
        h[i] = np.dot(theta, X_temp)
    h = h.reshape(X.shape[0])
    print(h)
    return h


def gradient_descent(X, y, initial_theta, alpha, number_iterations, degree):
    theta = initial_theta
    momentum = 0
    for k in range(0, number_iterations):
        h = calculate_h(theta, degree, X)
        gradient = step_gradient(X, y, h, degree)
        momentum = momentum + gradient
        for i in range(0, degree + 1):
            theta[i] = theta[i] - (((alpha) * gradient[i]) + momentum[i])
    return theta


def step_gradient(X, y, h, degree):
    gradient = np.zeros(degree + 1)
    for i in range(0, len(X)):
        for j in range(0, degree + 1):
            gradient[j] += 1 / len(X) * pow(X[i], j) * (h[i] - y[i])
    return gradient


def calculate_MSE(y, y_p):
    se = 0
    for i in range(0, len(y_p)):
        se += pow((y_p[i] - y[i]), 2)
    return se / len(y_p)







def run():
    data = pd.read_csv("data2_house_data.csv")
    data =data.drop(['date'], axis=1)
    corr = data.corr()
    fig = plt.figure()
    ax = fig.add_subplot(111)
    cax = ax.matshow(corr,cmap='coolwarm', vmin=-1, vmax=1)
    fig.colorbar(cax)
    ticks = np.arange(0,len(data.columns),1)
    ax.set_xticks(ticks)
    plt.xticks(rotation=90)
    ax.set_yticks(ticks)
    ax.set_xticklabels(data.columns)
    ax.set_yticklabels(data.columns)
    plt.show()

    

if __name__ == "__main__":
    run()

