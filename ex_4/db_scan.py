import math
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

UNCLASSIFIED = False
NOISE = 0


def dist(p, q):
    return math.sqrt(np.power(p - q, 2).sum())


def eps_neighborhood(p, q, eps):
    return dist(p, q) < eps


def region_query(m, point_id, eps):
    n_points = m.shape[1]
    seeds = []
    for i in range(n_points):
        if eps_neighborhood(m[:, point_id], m[:, i], eps):
            seeds.append(i)
    return seeds


def expand_cluster(m, classifications, point_id, cluster_id, eps, min_points):
    seeds = region_query(m, point_id, eps)
    if len(seeds) < min_points:
        classifications[point_id] = NOISE
        return False
    else:
        classifications[point_id] = cluster_id
        for seed_id in seeds:
            classifications[seed_id] = cluster_id
        while len(seeds) > 0:
            current_point = seeds[0]
            results = region_query(m, current_point, eps)
            if len(results) >= min_points:
                for i in range(0, len(results)):
                    result_point = results[i]
                    if classifications[result_point] == UNCLASSIFIED or classifications[result_point] == NOISE:
                        if classifications[result_point] == UNCLASSIFIED:
                            seeds.append(result_point)
                        classifications[result_point] = cluster_id
            seeds = seeds[1:]
        return True


def dbscan(m, eps, min_points):
    cluster_id = 1
    n_points = m.shape[1]
    classifications = [UNCLASSIFIED] * n_points
    for point_id in range(n_points):
        point = m[:, point_id]
        if classifications[point_id] == UNCLASSIFIED:
            if expand_cluster(m, classifications, point_id, cluster_id, eps, min_points):
                cluster_id = cluster_id + 1
    return classifications


from sklearn import metrics


def purity_score(y_true, y_pred):
    contingency_matrix = metrics.cluster.contingency_matrix(y_true, y_pred)
    return np.sum(np.amax(contingency_matrix, axis=0)) / np.sum(contingency_matrix)


if __name__ == '__main__':
    df = pd.read_csv('data/spiral.txt', header=None, sep='\t')
    X = np.array([df[0], df[1]])
    plt.scatter(X[0], X[1])
    plt.show()
    eps = 1.5
    min_points = 4
    labels = dbscan(X, eps, min_points)
    color_label = ['black']
    for i in range(0,len(labels)):
        color_label.append(np.random.rand(3, ))

    for i in range(0, len(labels)):
        plt.scatter(X[0][i], X[1][i], s=100, c=color_label[labels[i]])
    plt.show()
    print(purity_score(df[2], labels))
