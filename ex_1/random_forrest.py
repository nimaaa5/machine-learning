import pandas as pd
from sklearn.tree import DecisionTreeClassifier
import random
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix


def split_data(df_tr, df_tst):
    option1, option2, option3 = random.sample(range(0, 16), 3)
    x_tr = df_tr[[option1, option2, option3]].values
    x_tst = df_tst[[option1, option2, option3]].values

    return x_tr, x_tst


if __name__ == '__main__':
    df_train = pd.read_csv("data/data_train.csv", header=None)
    y_train = df_train.iloc[:, -1].values

    df_test = pd.read_csv("data/data_train.csv", header=None)
    y_test = df_test.iloc[:, -1].values

    y_pred_list = []
    for i in range(0, 15):
        x_train, x_test = split_data(df_train, df_test)
        classifier = DecisionTreeClassifier(max_depth=3)
        classifier.fit(x_train, y_train)
        y_pred_list.append(classifier.predict(x_test))

    y_pred = []
    dict_pred = {}
    for i in range(0, 10):
        dict_pred[i] = 0

    for i in range(0, len(y_pred_list[0])):
        for j in range(0, len(y_pred_list)):
            dict_pred[y_pred_list[j][i]] += 1
        y_pred.append(max(dict_pred, key=lambda k: dict_pred[k]))
        for j in range(0, 10):
            dict_pred[j] = 0

    print(accuracy_score(y_test, y_pred))
    print(confusion_matrix(y_test, y_pred))
