from collections import Counter

import numpy as np
import pandas as pd
from sklearn.metrics import accuracy_score
from sklearn.tree import DecisionTreeClassifier
import matplotlib.pyplot as plt


class AdaBoost:

    def __init__(self, classifier_num, K):
        self.dt_classifiers = []
        self.classifier_num = classifier_num
        self.K = K
        self.weight = 0
        self.y_pred = 0
        self.alpha = []
        self.error = 0

    def fit(self, x, y):
        self.weight, self.y_pred = self.initialize(x)

        for m in range(0, self.classifier_num):
            classifier = DecisionTreeClassifier(max_depth=2)
            classifier.fit(x, y, sample_weight=self.weight)
            self.y_pred = classifier.predict(x)
            incorrect = self.y_pred != y

            self.error = 0

            for i in range(0, len(x)):
                if incorrect[i]:
                    self.error += (self.weight[i])

            self.alpha.append(np.log((1. - self.error) / self.error) + np.log(self.K - 1.))
            for i in range(0, len(x)):
                if incorrect[i]:
                    self.weight[i] = self.weight[i] / (self.K * self.error)
                elif not incorrect[i]:
                    self.weight[i] = self.weight[i] / (self.K * (1. - self.error))

            self.dt_classifiers.append(classifier)

    def vote(self, predicts, weights=[]):
        voted_predict = []
        for i in range(len(predicts[0])):
            class_counter = Counter()
            for p in range(len(predicts)):
                if len(weights) == 0:
                    class_counter[predicts[p][i]] += 1
                else:
                    class_counter[predicts[p][i]] += weights[p]
            voted_predict.append(class_counter.most_common(1)[0][0])
        return voted_predict

    def predict(self, x_test):
        N = len(x_test)
        y_predict_list = []
        for model in self.dt_classifiers:
            y_predict = model.predict(x_test)
            y_predict_list.append(y_predict.copy())
        y_predict_list = np.asarray(y_predict_list)
        preds = self.vote(y_predict_list, self.alpha)
        return preds

    @staticmethod
    def initialize(x):
        x_len = len(x)
        return np.ones(x_len) / x_len, np.zeros(x_len)


if __name__ == '__main__':
    df_train = pd.read_csv("data/data_train.csv", header=None)
    df_test = pd.read_csv("data/data_test.csv", header=None)

    x_train = df_train.iloc[:, 0:16].values
    y_train = df_train.iloc[:, -1].values

    x_test = df_test.iloc[:, 0:16].values
    y_test = df_test.iloc[:, -1].values

    adaBoost_classifier = AdaBoost(100, 10)
    adaBoost_classifier.fit(x_train, y_train)
    y_pred = adaBoost_classifier.predict(x_test)
    print(accuracy_score(y_test, y_pred))

    indices = []
    for i in range(0, 100):
        indices.append(i)

    plt.plot(indices, adaBoost_classifier.alpha, c='red')
    plt.xlabel('Indices')
    plt.ylabel('Alpha (Classifier Weights)')
    plt.show()
