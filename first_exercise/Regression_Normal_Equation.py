
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb 23 08:06:07 2019

@author: nima
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from numpy.linalg import inv
from sklearn.model_selection import train_test_split




def hypothesis(theta, X, deg, x_array):
    h = np.ones((X.shape[0], 1))
    theta = theta.reshape(1, deg + 1)
    for i in range(0, X.shape[0]):
        x_array = np.ones(deg + 1)
        for j in range(0, deg + 1):
            x_array[j] = pow(X[i], j)
        x_array = x_array.reshape(deg + 1, 1)
        h[i] = float(np.matmul(theta, x_array))
    h = h.reshape(X.shape[0])
    return h


def get_theta(X, deg, _lambda):
    lambda_coeficient = get_lambda(_lambda, deg)
    array = np.ones((X.shape[0], deg + 1))
    for i in range(0, X.shape[0]):
        for j in range(0, deg + 1):
            array[i][j] = pow(X[i], j)
    theta = np.matmul(inv(np.matmul(array.transpose(), array)+lambda_coeficient),
                      np.matmul(array.transpose(), y_train))
    return array, theta

def get_lambda(_lambda, deg):
    ar = np.zeros((deg+1, deg+1))
    for i in range(1, deg+1):
        for j in range(1, deg+1):
            if i==j:
                ar[i][j] = 1
    ar = np.dot(_lambda, ar)
    return ar

def calculate_mse(training_predictions, y_train):
    se = 0 
    for i in range(0,y_train.shape[0]):
        se = se + pow((training_predictions[i] - y_train[i]), 2)
    return se/y_train.shape[0]






dataset = pd.read_csv('data1_Signal.csv')

X = dataset.iloc[:, 0:1].values
y = dataset.iloc[:, 1].values

X_train, X_test, y_train, y_test = train_test_split(X,y, test_size = .2)
_lambda = [5, 50, 500]
degree = [3,5,7]
mse=[0,0,0]
x_array_3, theta_3 = get_theta(X_train, 3, 0)
x_array_5, theta_5 = get_theta(X_train, 5,0)
x_array_7, theta_7 = get_theta(X_train, 7,0)

'''
x_array_7_5, theta_7_5 = get_theta(X_train, 7,_lambda[0])
x_array_7_50, theta_7_50 = get_theta(X_train, 7,_lambda[1])
x_array_7_500, theta_7_500 = get_theta(X_train, 7,_lambda[2])
'''
training_predictions_3 = hypothesis(theta_3, X_test, 3, x_array_3)
training_predictions_5 = hypothesis(theta_5, X_test, 5, x_array_5)
training_predictions_7 = hypothesis(theta_7, X_test, 7, x_array_7)

'''
training_predictions_7_5 = hypothesis(theta_7_5, X_test, 7, x_array_7_5)
training_predictions_7_50 = hypothesis(theta_7_50, X_test, 7, x_array_7_50)
training_predictions_7_500 = hypothesis(theta_7_500, X_test, 7, x_array_7_500)
scatter = plt.scatter(X_train, y_train, label="training data", color='grey')
'''

mse[0] = calculate_mse(training_predictions_3, y_test)
mse[1] = calculate_mse(training_predictions_5, y_test)
mse[2] = calculate_mse(training_predictions_7 , y_test)
plt.plot(degree, mse, label = 'MSE')
plt.xlabel('degree')
plt.ylabel('mse')

print()
plt.legend()
plt.xlabel('X Signal')
plt.ylabel('Y Signal')
















