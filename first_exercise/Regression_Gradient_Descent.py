import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split


def calculate_h(theta, degree, X):
    h = np.ones((len(X), 1))
    theta = theta.reshape(1, degree + 1)
    for i in range(0, len(X)):
        X_temp = np.ones(degree + 1)
        for j in range(0, degree + 1):
            X_temp[j] = pow(X[i], j)
        X_temp = X_temp.reshape(degree + 1, 1)
        h[i] = np.dot(theta, X_temp)
    h = h.reshape(X.shape[0])
    print(h)
    return h


def gradient_descent(X, y, initial_theta, alpha, number_iterations, degree):
    theta = initial_theta
    momentum = 0
    N = len(X)
    for k in range(0, number_iterations):
        h = calculate_h(theta, degree, X)
        gradient = step_gradient(X, y, h, degree)
        momentum = momentum + gradient
        for i in range(0, degree + 1):
            theta[i] = theta[i] - (((alpha) * gradient[i]) + momentum[i])
    return theta


def step_gradient(X, y, h, degree):
    gradient = np.zeros(degree + 1)
    for i in range(0, len(X)):
        for j in range(0, degree + 1):
            gradient[j] += 1 / len(X) * pow(X[i], j) * (h[i] - y[i])
    return gradient


def calculate_MSE(y, y_p):
    se = 0
    for i in range(0, len(y_p)):
        se += pow((y_p[i] - y[i]), 2)
    return se / len(y_p)


def correlation_matrix(df):

    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    cmap = plt.get_cmap('jet', 10)
    cax = ax1.imshow(df.corr(), interpolation="nearest", cmap=cmap)
    ax1.grid(True)
    plt.title('Feature Correlation')
    labels= df.iloc[0:1,10:18]
    print(labels)
    ax1.set_xticklabels(labels, fontsize=2)
    ax1.set_yticklabels(labels, fontsize=2)
    plt.show()




def run():
    dataset = pd.read_csv("data1_Signal.csv")
    X = dataset.iloc[:,0:1].values
    y = dataset.iloc[:,1].values
    X_train, X_test, y_train, y_test = train_test_split(X, y,test_size= .2)
    degree = [3,5,7]
    MSE = [0,0,0]

    
    alpha = 0.1
    num_iters = 100
    theta3 = np.ones(3 + 1)
    theta5 = np.ones(5 + 1)
    theta7 = np.ones(7 + 1)
    theta3 = gradient_descent(X_train, y_train, theta3, alpha, num_iters, 3)
    theta5 = gradient_descent(X_train, y_train, theta5, alpha, num_iters, 5)
    theta7 = gradient_descent(X_train, y_train, theta7, alpha, num_iters, 7)
    plt.scatter(X_train, y_train, label="training data", color='black')
    training3 = calculate_h(theta3, 3, X_test)
    #plt.plot(X_test, training3,label= "degree 3", color='red')
    training5 = calculate_h(theta5, 5, X_test)
    #plt.plot(X_test, training5,label= "degree 5", color='blue')
    training7 = calculate_h(theta7, 7,X_test)
    #plt.plot(X_test, training7,label= "degree7", color='green')
    #plt.legend()
    MSE[0] = calculate_MSE(y_test, training3)
    MSE[1] = calculate_MSE(y_test, training5)
    MSE[2] = calculate_MSE(y_test, training7)
    plt.plot(degree, MSE, label="10000 iteration")
    plt.xlabel('Degree')
    plt.ylabel('MSE')
    print(MSE[0])
    print(MSE[1])
    print(MSE[2])
    plt.show()


if __name__ == "__main__":
    run()

