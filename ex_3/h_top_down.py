import numpy as np
import pandas as pd
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt


class ClusterNode:
    def __init__(self, cluster_id, centroid, node_vector=None):
        self.cluster_id = cluster_id
        self.node_vector = node_vector
        self.centroid = centroid
        self.distance = 0
        self.is_split = False
        self.sse()

    def set_centroid(self):
        for n in self.node_vector:
            self.centroid[0] += n[0]
        self.centroid[0] /= len(self.node_vector)
        for n in self.node_vector:
            self.centroid[1] += n[1]
        self.centroid[1] /= len(self.node_vector)
        print('centroid ' + str(self.centroid))

    def sse(self):
        sse = 0
        for v in self.node_vector:
            sse += euclidean_distance(v, self.centroid)
        sse /= len(self.node_vector)
        self.distance = sse

    def minimum_distance(self):
        min_dist = 10000
        for i in range(0, len(self.node_vector)):
            for j in range(0, len(self.node_vector)):
                if i != j:
                    if min_dist > euclidean_distance(self.node_vector[i], self.node_vector[j]):
                        min_dist = euclidean_distance(self.node_vector[i], self.node_vector[j])
        self.distance = min_dist


def split_cluster(X, labels):
    zero_cluster = [X[i] for i, x in enumerate(labels) if x == 0]
    one_cluster = [X[i] for i, x in enumerate(labels) if x == 1]
    return zero_cluster, one_cluster


def euclidean_distance(vec1, vec2):
    return np.sqrt(sum((vec1 - vec2) ** 2))


def not_split_clusters(clusters):
    counter = 0
    for c in clusters:
        if c.is_split is False:
            counter += 1
    return counter


if __name__ == '__main__':
    df = pd.read_csv('data/data.csv', header=None)
    X = df.iloc[:, 0:2].values
    clusters = []
    kmeans = KMeans(n_clusters=2)
    kmeans.fit(X)
    zero_cluster, one_cluster = split_cluster(X, kmeans.labels_)
    j = 0
    clusters.append(ClusterNode(j, kmeans.cluster_centers_[0], zero_cluster))
    j += 1
    clusters.append(ClusterNode(j, kmeans.cluster_centers_[1], one_cluster))
    j += 1

    while (not_split_clusters(clusters) < 8):
        max_dissimilarity = 0
        cl = None
        for cluster in clusters:
            if cluster.is_split is False:
                if max_dissimilarity < cluster.distance:
                    max_dissimilarity = cluster.distance
                    cl = cluster
        kmeans.fit(cl.node_vector)
        cl.is_split = True
        zero_cluster, one_cluster = split_cluster(cl.node_vector, kmeans.labels_)
        clusters.append(ClusterNode(j, kmeans.cluster_centers_[0], zero_cluster))
        j += 1
        clusters.append(ClusterNode(j, kmeans.cluster_centers_[1], one_cluster))
        j += 1
    labels = []
    k = 0
    for cluster in clusters:
        if cluster.is_split is False:
            for node in cluster.node_vector:
                labels.append(k)
            k += 1

    print(labels)

    color_set = ['red', 'green', 'blue', 'yellow', 'brown', 'orange', 'black', 'cyan']
    l = 0
    for cluster in clusters:
        if cluster.is_split is False:
            for node in cluster.node_vector:
                plt.scatter(node[0], node[1], c=color_set[labels[l]])
                l += 1
    plt.show()
