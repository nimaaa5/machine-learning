import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

k = 8


class ClusterNode:
    def __init__(self, vec, id, left=None, right=None, distance=0.0, node_vector=None):
        self.left_node = left
        self.right_node = right
        self.vec = vec
        self.id = id
        self.distance = distance
        if node_vector is None:
            self.node_vector = [self.id]
        else:
            self.node_vector = node_vector[:]


def euclidean_distance(vec1, vec2):
    return np.sqrt(sum((vec1 - vec2) ** 2))


def single_link(clust1, clust2, distances):
    d = 12123123123123
    for i in clust1.node_vector:
        for j in clust2.node_vector:
            try:
                distance = distances[(i, j)]
            except:
                try:
                    distance = distances[(j, i)]
                except:
                    distance = euclidean_distance(clust1.vec, clust2.vec)
            if distance < d:
                d = distance
    return d


def complete_link(clust1, clust2, distances):
    d = -12123123123123
    for i in clust1.node_vector:
        for j in clust2.node_vector:
            try:
                distance = distances[(i, j)]
            except:
                try:
                    distance = distances[(j, i)]
                except:
                    distance = euclidean_distance(clust1.vec, clust2.vec)
            if distance > d:
                d = distance
    return d


def agglomerative_clustering(x, distance):
    distances = {}
    currentclustid = -1

    nodes = [ClusterNode(np.array(x[i]), id=i) for i in range(len(x))]

    while len(nodes) > k:
        lowestpair = (0, 1)
        closest = euclidean_distance(nodes[0].vec, nodes[1].vec)

        for i in range(len(nodes)):
            for j in range(i + 1, len(nodes)):
                if (nodes[i].id, nodes[j].id) not in distances:
                    if distance == "min":
                        distances[(nodes[i].id, nodes[j].id)] = single_link(nodes[i], nodes[j], distances)
                    if distance == "max":
                        distances[(nodes[i].id, nodes[j].id)] = complete_link(nodes[i], nodes[j], distances)
                    else:
                        distances[(nodes[i].id, nodes[j].id)] = euclidean_distance(nodes[i].vec, nodes[j].vec)

                d = distances[(nodes[i].id, nodes[j].id)]
                if d < closest:
                    closest = d
                    lowestpair = (i, j)

        len0 = len(nodes[lowestpair[0]].node_vector)
        len1 = len(nodes[lowestpair[1]].node_vector)
        mean_vector = [(len0 * nodes[lowestpair[0]].vec[i] + len1 * nodes[lowestpair[1]].vec[i]) / (len0 + len1) \
                       for i in range(len(nodes[0].vec))]

        new_node = ClusterNode(np.array(mean_vector), currentclustid, left=nodes[lowestpair[0]],
                               right=nodes[lowestpair[1]],
                               distance=closest,
                               node_vector=nodes[lowestpair[0]].node_vector + nodes[lowestpair[1]].node_vector)

        currentclustid -= 1
        del nodes[lowestpair[1]]
        del nodes[lowestpair[0]]
        nodes.append(new_node)

    return nodes


if __name__ == '__main__':
    df = pd.read_csv('data/data.csv', header=None)
    f = plt.figure(1)
    plt.scatter(df[0], df[1])
    f.show()
    color_set = ['red', 'green', 'blue', 'yellow', 'brown', 'orange', 'black', 'cyan']
    X = np.array(df)

    g = plt.figure(3)
    cluster = agglomerative_clustering(X, "min")

    j = 0
    for i in cluster:
        plt.scatter(X[i.node_vector].T[0], X[i.node_vector].T[1], color=color_set[j])
        j += 1
    g.show()
